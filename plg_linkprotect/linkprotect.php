<?php

/**
 * @package Joomla.Plugin
 * @subpackage Content.Joomla
 * @copyright NotByMe, but David Hurley You Tube 
 * @license GNU/GPU
 */
 
define('JEXEC') or die;

/**
 * Link protect Content Plugin
 * 
 * @package Joomla.Plugin
 * @subpackage Content.Joomla
 * @since 3.0
 */

class PlgContentLinkprotect extends Jplugin
{
    private $callbackFunction;
        
        public function __contruct(&$subject, $config = array())// De default Joomla Jplugin inregelen
        {
            parent::__construct($subject, $config); // De default Joomla Jplugin inregelen
            
            require_once __DIR__ . '/helper/helper.php';
            $helper = new LinkProtectHelper($this->params); // De Params (via de linkprotect.xml) kunnen gebruikt worden omdat deze al via JPlugin binnenkomen.
            $this->$callbackFunction = array($helper, 'replaceLinks'); // Hier wordt gecheckt vanuit de helper file en dan de function replaceLInks
        }
        /**
         * onContentBeforeDisplay Initiate de plugin   
         * @params String $context, De content die de plugin "aftrapt"
         * @params Object $artice,  Een Jobject die de artikel bevat
         * @params Object $params,  De artikel parameters. Dus anders dan de plugin params
         * 
         * @return Boolean True als de function is "ge-bypassed" Else/False gebasseerd op "vervang" actie
         */

        public function onContentBeforeDisplay($context, $article, $params) // (Joomla documentatie)context The context of the content being passed to the plugin - this is the component name and view - or name of module (e.g. com_content.article). Use this to check whether you are in the desired context for the plugin
        {
            $parts = explode(".",$context); //  Explode: Break a string into an array: De delimeter is dus een '.' ,  en dan de $context
            if($parts[0]!= "com_content") // dus hij wordt niet gerund als je niet in de juist com_content zitten
            {
                return;
            } 
            if(stripos($article->text, '{linkprotect=off}')== true)// hier wordt aangegeven om plugin per artikel uit te zetten
            {
                $article->text= str_replace('{linkprotect=off}','',$article->text);// mocht het uit staat, dan vervang het met de originele article tekst
            }
            $app = JFactory::getApplication(); // dus als bovenstaan goed is, (plugin runnen), dan gaan we gebruik maken van een stuk Joomla voeger Jrequest en nu $app
            $external = $app->input->get('external', NULL); // dus zoekek naar de var $external
            
            if($external){
                LinkProtectHelper::LeaveSite($article, $external);
            } else {
                $pattern = '`^((ftp|http|https):\/\/)?(\S+(:\S*)?@)?((([1-9]\d?|1\d\d|2[01]\d|22[0-3])(\.(1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.([0-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|((www\.)?)?(([a-z\x{00a1}-\x{ffff}0-9]+-?-?_?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.([a-z\x{00a1}-\x{ffff}]{2,}))?)|localhost)(:(\d{1,5}))?((\/|\?|#)[^\s]*)?$`';
                $article->text = preg_replace_callback($pattern, $this->$callbackFunction, $article->text);
            }
        }
    
    
    }

